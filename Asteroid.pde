public class Asteroid extends Transformable {

	public final static int DESTROY_ACCELERATION = 2;

	public final static int BIG = 50;

	public final static int MEDIUM = 30;

	public final static int SMALL = 10;

	private int numberStitches;

	private float[] offset;

	public Asteroid(float size) {
		this.setSize(size);
		this.numberStitches = floor(random(size + 10, size + 20));
		this.offset = new float[this.numberStitches];

		for (int i = 0; i < this.numberStitches; i++) {
			this.offset[i] = random(-2, 5);
		}
	}

	public Transformable update() {
		super.update();

		this.getPosition().add(this.getVelocity());

		return this;
	}

	public Transformable draw() {
		pushMatrix();
		translate(this.getPosition().x, this.getPosition().y);
		noStroke();
		fill(100);
		beginShape();
		for (int i = 0; i < this.numberStitches; i++) {
			float angle = map(i, 0, this.numberStitches, 0, TWO_PI);
			float sizePoint = this.getSize() + this.offset[i];
			vertex(sizePoint * cos(angle), sizePoint * sin(angle));
		}
		endShape(CLOSE);
		popMatrix();

		return this;
	}

	public float getScore() {
		return (150 / this.getSize());
	}
}