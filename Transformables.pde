public class Transformables {
	protected ArrayList<Transformable> objects;

	public Transformables() {
		this.objects = new ArrayList<Transformable>();
	}

	public ArrayList<Transformable> getObjects() {
		return this.objects;
	}

	public void draw() {
		for (Transformable object : this.objects) {
			object.draw();
		}
	}

	public void update() {
		for (Transformable object : this.objects) {
			object.update();
		}
	}
}