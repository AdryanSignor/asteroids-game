public class Part {

	private ArrayList<PVector> points;

	private color rgb;

	public Part() {
		this.points = new ArrayList();
		this.rgb = color(255, 255, 255);
	}

	public Part addPoint(PVector point) {
		this.points.add(point);

		return this;
	}

	public Part setColor(int rgb) {
		this.rgb = rgb;

		return this;
	}

	public void draw() {
		if (!this.points.isEmpty()) {
			fill(this.rgb);
			beginShape();
			for (PVector point : this.points) {
				vertex(point.x, point.y);
			}
			endShape(CLOSE);
		}
	}
}
