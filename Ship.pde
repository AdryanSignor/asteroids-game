public abstract class Ship extends Transformable {

	public static final float DEFAULT_SIZE = 20;

	public static final float MENU_SIZE = 50;

	public static final float ANGLE_SPIN = 0.1;

	private static final float VELOCITY_DEGRADATION = 0.98;

	private static final float VELOCITY_ANGLE_UTILIZATION = 0.1;

	protected ArrayList<Part> parts;

	private float angleSpin;

	private boolean moving;

	public Ship() {
		this.setPosition(new PVector(width / 2, height / 2));
		this.setSize(MENU_SIZE);

		this.setAngle(3 * PI / 2);
		this.setMoving(false);
		this.angleSpin = 0;
	}

	public Transformable draw() {
		pushMatrix();
		translate(this.getPosition().x, this.getPosition().y);
		rotate(this.getAngle() + PI / 2);
		noStroke();
		this.drawParts();
		popMatrix();

		return this;
	}

	public Transformable setSize(float size) {
		super.setSize(size);

		return this.updateDesign();
	}

	public Transformable update() {
		super.update();

		return this.spin().move();
	}

	public Ship setAngleSpin(float angleSpin) {
		this.angleSpin = angleSpin;

		return this;
	}

	public Transformable setMoving(boolean moving) {
		this.moving = moving;

		return this;
	}

	public boolean isMoving() {
		return this.moving;
	}

	public Ship damage() {
		if (!this.parts.isEmpty()) {
			this.parts.remove(this.parts.size() - 1);
		}

		return this;
	}

	protected abstract Part createBody();

	protected abstract Part createLeftWing();

	protected abstract Part createRightWing();

	protected abstract Part createShield();

	private Ship updateDesign() {
		this.parts = new ArrayList();

		this.parts.add(this.createBody().setColor(color(255, 0, 0)));
		this.parts.add(this.createLeftWing());
		this.parts.add(this.createRightWing());
		this.parts.add(this.createShield());

		return this;
	}

	private void drawParts() {
		for (Part part : this.parts) {
			part.draw();
		}
	}

	private Ship move() {
		if (this.isMoving()) {
			this.getVelocity().add(PVector.fromAngle(this.getAngle()).mult(this.VELOCITY_ANGLE_UTILIZATION));
		}

		this.getPosition().add(this.getVelocity());
		this.getVelocity().mult(this.VELOCITY_DEGRADATION);

		return this;
	}

	private Ship spin() {
		this.setAngle(this.getAngle() + this.angleSpin);

		return this;
	}
}
