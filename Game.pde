public class Game {

	private final static int KEY_SPACE = ' ';

	private final static int DRAW_MENU = 1;

	private final static int DRAW_GAME = 2;

	private final static int DRAW_END_GAME = 3;

	private final static int SHIP_TRIANGLE = 0;

	private final static int SHIP_TETRIS = 1;

	private final static int SHIP_THE_ONE = 2;

	private Ship ship;

	private Asteroids asteroids;

	private Shots shots;

	private PFont font;

	private int qtyAsteroids;

	private int score;

	private int qtyLifes;

	private int shipSelected;

	private int draw;

	public Game() {
		this.font = createFont("Arial", 25, true);
		this.shipSelected = 0;

		this.reset();
	}

	public void draw() {
		background(0);
		textFont(this.font);

		switch (this.draw) {
			case DRAW_MENU:
				this.drawMenu();
				break;

			case DRAW_GAME:
				this.drawGame();
				break;

			case DRAW_END_GAME:
				this.drawEndGame();
				break;
		}
	}

	public void verifyEventKeyReleased() {
		if (this.draw == DRAW_GAME) {
			if (keyCode == RIGHT || keyCode == LEFT) {
				ship.setAngleSpin(0);
			} if (keyCode == UP) {
				ship.setMoving(false);
			}
		}
	}

	public void verifyEventKeyPressed() {
		switch (this.draw) {
			case DRAW_MENU:
				this.eventKeyPressedMenu();
				break;

			case DRAW_GAME:
				this.eventKeyPressedGame();
				break;

			case DRAW_END_GAME:
				this.eventKeyPressedEndGame();
				break;
		}
	}

	private void reset() {
		this.changeShip();
		this.shots = new Shots();
		this.asteroids = new Asteroids();
		this.qtyAsteroids = 4;
		this.score = 0;
		this.draw = DRAW_MENU;
		this.qtyLifes = 4;
	}

	private void eventKeyPressedMenu() {
		if (keyCode == ENTER) {
			this.start();
		} else if (keyCode == RIGHT) {
			this.shipSelected++;
			this.changeShip();
		} else if (keyCode == LEFT) {
			if (--this.shipSelected < 0) {
				this.shipSelected = 2;
			}

			this.changeShip();
		}
	}

	private void eventKeyPressedGame() {
		if (keyCode == KEY_SPACE) {
			if (shots.canShoot()) {
				shots.createFromShip(ship);
			}
		} if (keyCode == UP) {
			ship.setMoving(true);
		} else if (keyCode == RIGHT) {
			ship.setAngleSpin(Ship.ANGLE_SPIN);
		} else if (keyCode == LEFT) {
			ship.setAngleSpin(-Ship.ANGLE_SPIN);
		}
	}

	private void eventKeyPressedEndGame() {
		if (keyCode == ENTER) {
			this.reset();
		}
	}

	private void drawMenu() {
		float height4 = height / 4.2;
		float height5 = height / 5;
		float heightCenter =  height / 2;
		float widthCenter = width / 2;

		fill(255);
		textAlign(CENTER);
		text("SELECIONE A ESPAÇONAVE", widthCenter, height5);
		text("← Use as setas →", widthCenter, height4);

		this.ship.draw();

		fill(255);
		text("COMANDOS:", widthCenter, heightCenter + height5);
		text("Direção: ← → | Aceleração: ↑ | Atirar: Espaço", widthCenter, heightCenter + height4);
		text("Pressione enter para iniciar", widthCenter, heightCenter + height / 3.2);
	}

	private void drawGame() {
		this.update();

		strokeWeight(2);
		this.asteroids.draw();
		this.shots.draw();
		this.ship.draw();

		fill(255);
		textAlign(LEFT);
		text(nf(this.score, 8), 5, 30);

		String lifes = "";
		for (int i = 0; i < this.qtyLifes; i++) {
			lifes += '❤';
		}

		text(lifes, 150, 30);
	}

	private void drawEndGame() {
		float heightCenter =  height / 2;
		float widthCenter = width / 2;

		fill(255);
		textAlign(CENTER);
		text("FIM DE JOGO!", widthCenter, heightCenter - 40);
		text("Você marcou " + nf(this.score, 8) + " pontos", widthCenter, heightCenter);
		text("Pressione enter para novo jogo ou esc para sair", widthCenter, heightCenter + 40);
	}

	private void start() {
		this.draw = DRAW_GAME;
		this.ship.setSize(Ship.DEFAULT_SIZE);
	}

	private void update() {
		if (this.asteroids.getObjects().size() == 0) {
			this.asteroids.create(this.qtyAsteroids);
			this.qtyAsteroids += 2;
		}

		this.asteroids.update();
		this.shots.update();
		this.ship.update();

		this.verifyHits();
		this.shots.deleteShotsExpired();
	}

	private void verifyHits() {
		ArrayList<Asteroid> asteroidsHit = this.shots.verifyAsteroidHit(this.asteroids.getObjects());

		this.updateScore(asteroidsHit);
		this.asteroids.destroy(asteroidsHit);

		if (this.asteroids.verifyAsteroidsHitShip(this.ship)) {
			this.qtyLifes--;
			this.ship.damage();
			if (this.qtyLifes == 0) {
				this.draw = DRAW_END_GAME;
			}
		}
	}

	private void updateScore(ArrayList<Asteroid> asteroidsHit) {
		for (Asteroid asteroid : asteroidsHit) {
			this.score += asteroid.getScore();
		}
	}

	private void changeShip() {
		switch (this.shipSelected % 3) {
			case SHIP_TRIANGLE:
				this.ship = new ShipTriangle();
				break;

			case SHIP_TETRIS:
				this.ship = new ShipTetris();
				break;

			case SHIP_THE_ONE:
				this.ship = new ShipTheOne();
				break;
		}
	}
}