public class ShipTriangle extends Ship {
	protected Part createBody() {
		return new Part().addPoint(new PVector(-this.getSize(), this.getSize()))
			.addPoint(new PVector(0, -this.getSize()))
			.addPoint(new PVector(this.getSize(), this.getSize()));
	}

	protected Part createLeftWing() {
		return new Part().addPoint(new PVector(-this.getSize(), this.getSize()))
			.addPoint(new PVector(0, -this.getSize()))
			.addPoint(new PVector(0, this.getSize()));
	}

	protected Part createRightWing() {
		return new Part().addPoint(new PVector(this.getSize(), this.getSize()))
			.addPoint(new PVector(0, -this.getSize()))
			.addPoint(new PVector(0, this.getSize()));
	}

	protected Part createShield() {
		return this.createBody();
	}
}