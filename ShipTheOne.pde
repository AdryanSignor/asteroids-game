public class ShipTheOne extends Ship {
	protected Part createBody() {
		float size3 = this.getSize() / 3;
		float size2P3 = 2 * this.getSize() / 3;

		return new Part().addPoint(new PVector(-size3, -this.getSize()))
			.addPoint(new PVector(0, -size3))
			.addPoint(new PVector(size3, -this.getSize()))
			.addPoint(new PVector(size3, -size3))
			.addPoint(new PVector(this.getSize(), size3))
			.addPoint(new PVector(size3, this.getSize()))
			.addPoint(new PVector(0, size2P3))
			.addPoint(new PVector(-size3, this.getSize()))
			.addPoint(new PVector(-this.getSize(), size3))
			.addPoint(new PVector(-size3, -size3))
		;
	}

	protected Part createLeftWing() {
		float size3 = this.getSize() / 3;

		return new Part().addPoint(new PVector(size3, -size3))
			.addPoint(new PVector(this.getSize(), size3))
			.addPoint(new PVector(size3, this.getSize()));
	}

	protected Part createRightWing() {
		float size3 = this.getSize() / 3;

		return new Part().addPoint(new PVector(-size3, this.getSize()))
			.addPoint(new PVector(-this.getSize(), size3))
			.addPoint(new PVector(-size3, -size3));
	}

	protected Part createShield() {
		return this.createBody();
	}
}