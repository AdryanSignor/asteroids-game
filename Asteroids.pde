public class Asteroids extends Transformables {

	public void create(int qty) {
		for (int i = 0; i < qty; i++) {
			this.createFromRandom();
		}
	}

	public void createFromRandom() {
		float angle = random(2 * PI);
		this.add(Asteroid.BIG, new PVector(random(width), random(height)), PVector.fromAngle(angle), angle);
	}

	public void createFromHit(Asteroid hit) {
		float size = (hit.getSize() == Asteroid.BIG ? Asteroid.MEDIUM : (hit.getSize() == Asteroid.MEDIUM ? Asteroid.SMALL: 0));

		if (size > 0) {
			float deviationAngle = PI / 8;

			for (int i = 0; i < 2; i++) {
				float angle = hit.getAngle() + deviationAngle;

				this.add(size, hit.getPosition().copy(), PVector.fromAngle(angle).mult(Asteroid.BIG / hit.getSize() * Asteroid.DESTROY_ACCELERATION), angle);

				deviationAngle *= -1;
			}
		}
	}

	public void destroy(ArrayList<Asteroid> asteroids) {
		for (Asteroid asteroid : asteroids) {
			this.createFromHit(asteroid);
			this.objects.remove(asteroid);
		}
	}

	public boolean verifyAsteroidsHitShip(Ship ship) {
		ArrayList<Transformable> asteroidsHit = new ArrayList();
		boolean hits = false;

		for (Transformable asteroid : this.objects) {
			if (this.verifyHitShip(ship.getPosition(), asteroid.getPosition(), ship.getSize(), asteroid.getSize())) {
				hits = true;
				asteroidsHit.add(asteroid);
			}
		}

		for (Transformable asteroid : asteroidsHit) {
			this.objects.remove(asteroid);
		}

		return hits;
	}

	private void add(float size, PVector position, PVector velocity, float angle) {
		this.objects.add(
			new Asteroid(size)
			.setPosition(position)
			.setVelocity(velocity)
			.setAngle(angle)
		);
	}

	private boolean verifyHitShip(PVector shipPosition, PVector asteroidPosition, float shipSize, float asteroidSize) {
		return (dist(shipPosition.x, shipPosition.y, asteroidPosition.x, asteroidPosition.y) < shipSize + asteroidSize);
	}
}