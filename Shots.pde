public class Shots extends Transformables {

	private final static int TIME_SHOOT = 100;

	public void createFromShip(Ship ship) {
		this.objects.add(
			new Shot()
			.setPosition(ship.getPosition().copy())
			.setVelocity(PVector.fromAngle(ship.getAngle()).mult(Shot.VELOCITY_ACCELERATION))
			.setAngle(ship.getAngle())
		);
	}

	public ArrayList<Asteroid> verifyAsteroidHit(ArrayList<Transformable> asteroids) {
		ArrayList<Asteroid> asteroidsHit = new ArrayList();
		ArrayList<Transformable> shotsHit = new ArrayList();

		for (Transformable shot : this.objects) {
			for (Transformable asteroid : asteroids) {
				if (this.verifyShotsInsideAsteroid(shot.getPosition(), asteroid.getPosition(), asteroid.getSize())) {
					shotsHit.add(shot);
					asteroidsHit.add((Asteroid) asteroid);
				}
			}
		}

		for (Transformable shotHit : shotsHit) {
			this.objects.remove(shotHit);
		}

		return asteroidsHit;
	}

	public boolean canShoot() {
		boolean can = true;

		if (!this.objects.isEmpty()) {
			can = (millis() - ((Shot) this.objects.get(this.objects.size() - 1)).getCreateTime() > TIME_SHOOT);
		}

		return can;
	}

	public void deleteShotsExpired() {
		ArrayList<Transformable> shotsExpired = new ArrayList();
		for (Transformable shot : this.objects) {
			if (((Shot) shot).isExpired()) {
				shotsExpired.add(shot);
			}
		}

		for (Transformable shotExpired : shotsExpired) {
			this.objects.remove(shotExpired);
		}
	}

	private boolean verifyShotsInsideAsteroid(PVector shotPosition, PVector asteroidPosition, float asteroidSize) {
		return (dist(shotPosition.x, shotPosition.y, asteroidPosition.x, asteroidPosition.y) < asteroidSize);
	}
}