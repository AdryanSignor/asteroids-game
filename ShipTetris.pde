public class ShipTetris extends Ship {
	protected Part createBody() {
		float size3 = this.getSize() / 3;

		return new Part().addPoint(new PVector(-this.getSize(), this.getSize()))
			.addPoint(new PVector(-this.getSize(), size3))
			.addPoint(new PVector(-size3, size3))
			.addPoint(new PVector(-size3, -size3))
			.addPoint(new PVector(0, -getSize()))
			.addPoint(new PVector(size3, -size3))
			.addPoint(new PVector(size3, size3))
			.addPoint(new PVector(this.getSize(), size3))
			.addPoint(new PVector(this.getSize(), this.getSize()));
	}

	protected Part createLeftWing() {
		float size3 = this.getSize() / 3;

		return new Part().addPoint(new PVector(-this.getSize(), this.getSize()))
			.addPoint(new PVector(-this.getSize(), size3))
			.addPoint(new PVector(-size3, size3))
			.addPoint(new PVector(-size3, this.getSize()));
	}

	protected Part createRightWing() {
		float size3 = this.getSize() / 3;

		return new Part().addPoint(new PVector(this.getSize(), this.getSize()))
			.addPoint(new PVector(this.getSize(), size3))
			.addPoint(new PVector(size3, size3))
			.addPoint(new PVector(size3, this.getSize()));
	}

	protected Part createShield() {
		return this.createBody();
	}
}