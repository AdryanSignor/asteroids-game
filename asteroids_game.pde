Game game;

void setup() {
	// fullScreen();
	size(800, 600);
	game = new Game();
}

void draw() {
	game.draw();
}

void keyReleased() {
	game.verifyEventKeyReleased();
}

void keyPressed() {
	game.verifyEventKeyPressed();
}