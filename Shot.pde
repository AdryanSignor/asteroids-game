public class Shot extends Transformable {

	public final static float VELOCITY_ACCELERATION = 5.5;

	private final static float EXPIRATION_TIME = 3000;

	private int createTime;

	public Shot() {
		super();
		this.setSize(1);

		this.createTime = millis();
	}

	public Transformable update() {
		super.update();
		this.getPosition().add(this.getVelocity());

		return this;
	}

	public Transformable draw() {
		pushMatrix();
		stroke(255);
		fill(255, 0, 0);
		ellipse(this.getPosition().x, this.getPosition().y, this.getSize() * 2, this.getSize() * 2);
		point(this.getPosition().x, this.getPosition().y);
		popMatrix();

		return this;
	}

	public boolean isExpired() {
		return (millis() - this.createTime > EXPIRATION_TIME);
	}

	public int getCreateTime() {
		return this.createTime;
	}
}
