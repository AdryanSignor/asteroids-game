public abstract class Transformable {

	private PVector position;

	private PVector velocity;

	private float size;

	private float angle;

	public Transformable() {
		this.setPosition(new PVector(0, 0));
		this.setVelocity(new PVector(0, 0));
		this.setSize(0);
		this.setAngle(0);
	}

	public Transformable setPosition(PVector position) {
		this.position = position;

		return this;
	}

	public PVector getPosition() {
		return this.position;
	}

	public Transformable setVelocity(PVector velocity) {
		this.velocity = velocity;

		return this;
	}

	public PVector getVelocity() {
		return this.velocity;
	}

	public Transformable setAngle(float angle) {
		this.angle = angle;

		return this;
	}

	public float getAngle() {
		return this.angle;
	}

	public abstract Transformable draw();

	protected Transformable setSize(float size) {
		this.size = size;

		return this;
	}

	protected float getSize() {
		return this.size;
	}

	protected Transformable update() {
		return this.checkEdges();
	}

	private Transformable checkEdges() {
		if (this.position.x > width + this.size) {
			this.position.x = -this.size;
		} else if (this.position.x < -this.size) {
			this.position.x = width + this.size;
		}

		if (this.position.y > height + this.size) {
			this.position.y = -this.size;
		} else if (this.position.y < -this.size) {
			this.position.y = height + this.size;
		}

		return this;
	}
}